<?php
/**
 *
 * @author:     Askold Beyer <askold.beyer@shopping.de>
 * @date:       24.03.17
 * @copyright   Copyright (c) 2017 solute.de GmbH
 */

class Solute_Sop_TestController extends Mage_Core_Controller_Front_Action
{

    public function testAction()
    {
        die('...');
        /** @var Solute_Sop_Model_Service_Order $orderService */
        $orderService = Mage::getModel('solute_sop/service_order');
        $client = $orderService->getApiClient();


        $request = $orderService->getNewOrderRequest();
        $request->setOrderId(555);
        $sopOrder = $client->getOrder($request);
        $sopOrder->setEmail('shopping@billiger.de');

        #$orderService->saveSopOrder($sopOrder);

        Zend_Debug::dump($sopOrder);

        die();


        $request = $orderService->getNewOrdersRequest();
        $request->setConfirmationState('none')->setCanceledState('none');
        $response = $client->getOrders($request);
        #\Ubl\Core\Debug::dump($response);
        foreach ($response->getAll() as $order ) {
            /** @var \SoluteSop\Api\Response\Order $order */
            foreach ($order->getItems() as $item) {
                /** @var \SoluteSop\Api\Response\OrderItem $item */
                echo('(OrderID: ' . $order->getOrderId() . ' CartRef: ' . $order->getCartReference() . ') LineRef: ' . $item->getLineReference() . ' (Qty C-' . $item->getQuantityConfirmed() . ' | O-' . $item->getQuantityOrdered(). ' | S-' . $item->getQuantityShipped() . ')<br>');

            }
        }



        die();

        $linerefs = array(
            '602_1_1-1',
            '604_1_1-1',
            '603_1_1-1',
            '2399_1_1-1',
            '605_1_1-1'
        );

        foreach ($linerefs as $lineRef) {
            // Test Update OrderLine
            #$lineRef = '';

            $request = $orderService->getNewOrderLineRequest();
            $request->setQuantityConfirmed(1)->setLineReference($lineRef);

            $response = $client->updateOrderLine($request);

            \Ubl\Core\Debug::dump($response);

        }


    }
}
