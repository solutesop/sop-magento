<?php

/**
 *
 *
 * @desc Helper needed for admin settings
 *
 * @category   Solute
 * @package    Solute_Sop
 * @subpackage helper
 * @copyright  Copyright (c) 2017 Solute Gmbh
 * @since      release 1.0.0
 * @author     a.beyer
 *
 */
class Solute_Sop_Helper_Data extends Mage_Core_Helper_Abstract
{


}
