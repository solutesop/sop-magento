# Solute SOP Magento Extension

## Systemvoraussetzungen
- PHP >= 5.4
- Magento >= 1.9.3.2
- PHP Lib Curl (php-curl)

## Installation
System -> Konfiguration -> Verkauf -> Solute SOP

### API Einstellungen
- API einschalten
- den von SOP bereitgestellten API Access Token eintragen

### Bestelleinstellungen
- Valide Email eines Kunden eintragen, über den alle Bestellungen laufen sollen
- Hintergrund: SOP liefert derzeitig nur eine Email zurück, unterschiedlich sind nur die Versandadressen
- valide Versandart auswählen
- valide Zahlart auswählen

## Informationen für Entwickler / Debugging
- Log Datei var/log/solute-sop.log
- sollten Fehler auftreten, gibt es unter oben genannten Logfile genauere Informationen
    - nach jedem "saveSopOrder Start" sollte auch ein "saveSopOrder End" folgen

## Problembehebung
### Bestellungen laufen nicht im System ein unter eingestellem Kunden
- Wenn Warenkörbe vorhanden, dann gibt es wahrscheinlich ein Problem mit der ausgewählten Versandart, Zahlart oder mit der Rechnungsadresse
- Produkte im Store nicht vorhanden unter der exportierten SKU
- Produkte nicht auf Lager (Workarround, Bestellungen zulassen, bei denen Produkte nicht lagernd sind)

### Bestellungen laufen nicht automatisch ein
- Cron prüfen




