<?php
/**
 *
 *
 * @desc Setup declaration
 *
 */

/**
 *
 * @author:     Askold Beyer <askold.beyer@shopping.de>
 * @date:       2017-11-30
 * @copyright   Copyright (c) 2017 Shopping.de Handels GmbH
 */

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$sql = "ALTER TABLE " . $installer->getTable('sales/quote') . "
ADD INDEX `IDX_SALES_FLAT_QUOTE_INCREMENT_ID` (`reserved_order_id` ASC);
";
$installer->run($sql);

$installer->endSetup();
