<?php
class Solute_Sop_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

	protected $_code = 'solute_sop';

    public function isAvailable($quote = null)
    {
        /** @var Solute_Sop_Model_Service_Order $orderService */
        $orderService = Mage::getModel('solute_sop/service_order');
        if ($orderService->isSopQuote($quote)) {
            return true;
        }
        return parent::isAvailable($quote);
    }

}