<?php
$baseDire = Mage::getBaseDir('base');
$autoloaderPath = $baseDire . '/lib/solutesop/sop-api-client/src/Autoloader.php';

if (file_exists($autoloaderPath)) {
    include_once $autoloaderPath;
}

class Solute_Sop_Model_Service_Abstract
{

	const LOG_FILE = 'solute_sop.log';

	const XML_PATH_SOLUTE_SOP_SETTING_API_ENABLED = 'solute_sop_setting/api/enabled';
	const XML_PATH_SOLUTE_SOP_SETTING_API_ACCESS_TOKEN = 'solute_sop_setting/api/access_token';
	const XML_PATH_SOLUTE_SOP_SETTING_API_VERSION = 'solute_sop_setting/api/version';

	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_ENABLED = 'solute_sop_setting/sync/enabled';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_CUSTOMER_EMAIL = 'solute_sop_setting/sync/customer_email';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_PAYMENT_METHOD = 'solute_sop_setting/sync/payment_method';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_SHIPPING_METHOD = 'solute_sop_setting/sync/shipping_method';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_CURRENCY_CODE = 'solute_sop_setting/sync/currency_code';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_CUSTOMER_GROUP = 'solute_sop_setting/sync/customer_group';
	const XML_PATH_SOLUTE_SOP_SETTING_SYNC_ORDER_INCREMENT_ID_PREFIX = 'solute_sop_setting/sync/order_increment_id_prefix';
    const XML_PATH_SOLUTE_SOP_SETTING_SYNC_ORDER_USE_CARTREFERENCE_AS_INCREMENTID = 'solute_sop_setting/sync/order_use_cartreference_as_increment_id';

	/**
	 *
	 * @var Solute_Sop_Model_Config
	 */
	protected $_config = null;

	final public function __construct()
	{
		$this->_initConfig();

		$this->_construct();
	}

	/**
	 *
	 * @return Solute_Sop_Model_Service_Abstract
	 */
	protected function _construct()
	{
		return $this;
	}

	protected function _initConfig()
	{
		$config = $this->getConfig();
		$pathes = array(
				self::XML_PATH_SOLUTE_SOP_SETTING_API_ACCESS_TOKEN => 'apiAccessToken',
				self::XML_PATH_SOLUTE_SOP_SETTING_API_ENABLED => 'apiEnabled',
				self::XML_PATH_SOLUTE_SOP_SETTING_API_VERSION => 'apiVersion',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_CURRENCY_CODE => 'syncCurrencyCode',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_CUSTOMER_EMAIL => 'syncCustomerEmail',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_CUSTOMER_GROUP => 'syncCustomerGroup',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_ENABLED => 'syncEnabled',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_PAYMENT_METHOD => 'syncPaymentMethod',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_SHIPPING_METHOD => 'syncShippingMethod',
				self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_ORDER_INCREMENT_ID_PREFIX => 'syncOrderIncrementIdPrefix',
                self::XML_PATH_SOLUTE_SOP_SETTING_SYNC_ORDER_USE_CARTREFERENCE_AS_INCREMENTID => 'syncOrderUseCartreferenceAsIncrementId'
		);

		foreach ($pathes as $path => $key) {

			$method = 'set' . ucfirst($key);
			$value = Mage::getStoreConfig($path);
			#Zend_Debug::dump($value,$key);
			$config->$method($value);
		}

		return $this;
	}

	public function log($message)
	{
		#Zend_Debug::dump($message);
		Mage::log($message, null, self::LOG_FILE, true);
		return $this;
	}


	/**
	 *
	 * @param Solute_Sop_Model_Config $value
	 * @return Solute_Sop_Model_Service_Abstract
	 */
	public function setConfig(Solute_Sop_Model_Config $value)
	{
		$this->_config = $value;
		return $this;
	}
	/**
	 *
	 * @return Solute_Sop_Model_Config
	 */
	public function getConfig()
	{
		if ($this->_config === null) {
			$this->_config = Mage::getModel('solute_sop/config');
		}
		return $this->_config;
	}


}
