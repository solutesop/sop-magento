<?php
use SoluteSop\Api\Client;
use SoluteSop\Api\Request\Orders;
use SoluteSop\Api\Request\OrderLine;
use SoluteSop\Api\Response\OrderItem;
use SoluteSop\Api\Request\Order;

class Solute_Sop_Model_Service_Order extends Solute_Sop_Model_Service_Abstract
{

	const VARIABLE_CODE_ORDER_CRON_LAST_RUN = 'solute_sop_order_cron_last_run';


	const ORDER_LINE_REFERENCE_KEY = 'soluteSopOrderLineReference';

	const ORDER_INCREMENT_ID_PREFIX_FALLBACK = 'SOP';
	const ORDER_INCREMENT_ID_DELIMITER = '_';

	const ORDER_STATE_CONFIRM = 'confirm';
	const ORDER_STATE_SHIP = 'ship';

	/**
	 *
	 * @var Client
	 */
	protected $_apiClient = null;

	/**
	 *
	 * @var Mage_Core_Model_Variable
	 */
	protected $_cronLastRunVariable = null;

	public function getIncrementId(\SoluteSop\Api\Response\Order $orderResponse)
	{
        if ($this->getConfig()->getSyncOrderUseCartreferenceAsIncrementId()) {
            return $orderResponse->getCartReference();
        }

		$prefix = $this->getConfig()->getSyncOrderIncrementIdPrefix();
		if (!$prefix) {
			$prefix = self::ORDER_INCREMENT_ID_PREFIX_FALLBACK;
		}
		return $prefix . self::ORDER_INCREMENT_ID_DELIMITER . $orderResponse->getSupplierId() . self::ORDER_INCREMENT_ID_DELIMITER . $orderResponse->getOrderId();

	}

	/**
	 *
	 * @param Mage_Sales_Model_Quote_Item $quoteItem
	 * @param OrderItem $orderItemResponse
	 * @return Solute_Sop_Model_Service_Order
	 */
	public function setSopOrderLineReferenceToQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem, OrderItem $orderItemResponse)
	{
		// Additional Data, save needed lineReference to Quote -> Order to update
		$additionalData = unserialize($quoteItem->getAdditionalData());
		if (!is_array($additionalData)) {
			$additionalData = array();
		}
		$additionalData[self::ORDER_LINE_REFERENCE_KEY] = $orderItemResponse->getLineReference();
		$quoteItem->setAdditionalData(serialize($additionalData));

		return $this;
	}

	public function getSopOrderIdByOrder(Mage_Sales_Model_Order $order)
	{
		$incrementId = $order->getIncrementId();
		$parts = explode(self::ORDER_INCREMENT_ID_DELIMITER, $incrementId);

		if (!isset($parts[2])) {
			throw new RuntimeException('SopOrderId couldnt be extracted by incrementId "' . $incrementId . '"');
		}

		return $parts[2];
	}

	/**
	 *
	 * @param Mage_Sales_Model_Order_Item $orderItem
	 * @throws UnexpectedValueException
	 * @return int
	 */
	public function getSopOrderLineReferenceByOrderItem(Mage_Sales_Model_Order_Item $orderItem)
	{
		$additionalData = unserialize($orderItem->getAdditionalData());

		if (!isset($additionalData[self::ORDER_LINE_REFERENCE_KEY])) {
			throw new UnexpectedValueException(self::ORDER_LINE_REFERENCE_KEY . ' not set in additionalData of order item');
		}
		return (string) $additionalData[self::ORDER_LINE_REFERENCE_KEY];
	}



	/**
	 * @return \SoluteSop\Api\Response\Orders
	 */
	public function getSopOrders(DateTime $dateFrom = null, Orders $request = null)
	{
		if ($request === null) {
			$request = $this->getNewOrdersRequest();
		}

		if ($dateFrom) {
			$request->setDateFrom($dateFrom);
		}

		return $this->getApiClient()->getOrders($request);
	}

	/**
	 *
	 * @param Mage_Sales_Model_Order $order
	 * @throws UnexpectedValueException
	 * @throws RuntimeException
	 * @return \SoluteSop\Api\Request\Order
	 */
	public function getSopOrder(Mage_Sales_Model_Order $order)
	{
		try {

			if (!$this->isSopOrder($order)) {
				throw new UnexpectedValueException('Order "' . $order->getIncrementId() . '" is no SOP Order');
			}

			$request = $this->getNewOrderRequest();

			$sopOrderId = $this->getSopOrderIdByOrder($order);

			$request->setOrderId($sopOrderId);

			return $this->getApiClient()->getOrder($request);

		} catch (Exception $e) {
			throw new RuntimeException('Order::getOrder failed (' . $e->getMessage() . ')', null, $e);
		}
	}

	/**
	 * @see Solute_Sop_Model_Observer::handleOrderAfterSave
	 * @param Mage_Sales_Model_Order $order
	 * @throws RuntimeException
	 * @return Solute_Sop_Model_Service_Order
	 */
	public function handleOrderUpdate(Mage_Sales_Model_Order $order)
	{
		try {
            if (!$this->getConfig()->getApiEnabled()) {
                return $this;
            }

			$this->log('handleOrderUpdate :: ' . $order->getIncrementId() . ' :: Start');
			if (!$this->isSopOrder($order)) {
				#$this->log('handleOrderUpdate :: ' . $order->getIncrementId() . ' :: no sop order -> return');
				return $this;
			}
			// we have a sop order, so look for something to do

			if ($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE
			    || $order->getStatus() == Mage_Sales_Model_Order::STATE_COMPLETE
			) {
				$this->updateOrder($order, self::ORDER_STATE_SHIP);
				$this->log('handleOrderUpdate :: ' . $order->getIncrementId() . ' :: Marked as shipped');
			} else {
				$this->log('handleOrderUpdate :: ' . $order->getIncrementId() . ' :: Nothing todo');
			}


			return $this;

		} catch (Exception $e) {
		    $this->log($e->getMessage());
			throw new RuntimeException('Order::handleOrderUpdate failed (' . $e->getMessage() . ')', null, $e);
		}
	}


	/**
	 *
	 * @param Mage_Sales_Model_Order $order
	 * @param unknown $state
	 * @param OrderLine $request
	 * @throws UnexpectedValueException
	 * @throws RuntimeException
	 * @return Solute_Sop_Model_Service_Order
	 */
	public function updateOrder(Mage_Sales_Model_Order $order, $state = self::ORDER_STATE_CONFIRM, OrderLine $request = null)
	{
		try {
            if (!$this->getConfig()->getApiEnabled()) {
                return $this;
            }

			$trackingNumber = array();
            $carrier = array();
            if ($order->getShippingCarrier() && $order->getShippingCarrier()->getCarrierCode()) {
                $carrier['_fallback'] = $order->getShippingCarrier()->getCarrierCode();
            }

            try {
                /** @var $shipment Mage_Sales_Model_Order_Shipment */
                $shipmentCollection = $order->getShipmentsCollection();
                foreach ($shipmentCollection as $shipment) {

                    #$shipment = Mage::getModel('sales/order_shipment')->loadByIncrementId($order->getIncrementId());
                    #Zend_Debug::dump($shipment->debug());
                    foreach ($shipment->getAllTracks() as $track) {
                        /** @var $track Mage_Sales_Model_Order_Shipment_Track */

                        #Zend_Debug::dump(get_class($track));

                        $carrier[$track->getTitle()] = $track->getTitle(); // took the last carrier
                        $trackingNumber[$track->getNumber()] = $track->getNumber(); // if exists, default magento exists these rows and can be multiple
                    }

                }

                if (count($carrier) > 1 && isset($carrier['_fallback'])) {
                    // we found a better carrier description by shipment collection
                    unset($carrier['_fallback']);
                }
                #Zend_Debug::dump($trackingNumber);
                #Zend_Debug::dump($carrier);

            } catch (Exception $e) {
                // wir machen erstmal nichts
                Mage::logException($e);
            }

            #die('ASKOLD ENDE');


			foreach ($order->getAllVisibleItems() as $item) {
				/** @var Mage_Sales_Model_Order_Item $item */
				$lineReference = $this->getSopOrderLineReferenceByOrderItem($item);

				if (!$lineReference > 0) {
					throw new UnexpectedValueException('Line Reference not set in additionalData ' . $order->getId());
				}

				if ($request === null) { // dosnt make sense soooooo lot, beceause, just for preset a request object
					$request = $this->getNewOrderLineRequest();
				}

				$request->setLineReference($lineReference);

				switch ($state) {
					case self::ORDER_STATE_CONFIRM:

						$request->setQuantityConfirmed($item->getQtyOrdered());

						break;

					case self::ORDER_STATE_SHIP:

					    $request->setQuantityShipped($item->getQtyShipped());
						$request->setShippingCarrier(implode(', ', $carrier));
						$request->setShippingCarrierTrackingId(implode(', ', $trackingNumber));

						break;
					default:

						throw new InvalidArgumentException('Order::updateOrder() failed. State "' . (string) $state . '" unknown');

						break;
				}

				#Zend_Debug::dump($request);
                #$this->log($request);
                #die('died -> ' . __FILE__ . ' :: ' . __LINE__ . PHP_EOL);
				$response = $this->getApiClient()->updateOrderLine($request);

				$errorMsg = '';
				if ($response->getError()) {
				    $errorMsg = ' API-Error: "' . $response->getError() . '"';
				}

				$history = $order->addStatusHistoryComment('Sop OrderItem StatusChange ' . (string) $state . '(id:' . $lineReference . ')' . $errorMsg, false);
                if ($history) {
                    $history->save();
                }

                $this->log('Sop OrderItem StatusChange ' . (string) $state . '(id:' . $lineReference . ')');
                $this->log($request);
                $this->log($response);

                // @todo response auswerten

			}
			return $this;

		} catch (Exception $e) {
		    $this->log($e->getMessage());
			throw new RuntimeException('Order::updateOrder failed (' . $e->getMessage() . ')', null, $e);
		}
	}


	public function cancelSopOrders()
	{
	    try {
	        if (!$this->getConfig()->getApiEnabled()) {
	            return $this;
	        }

	        $request = $this->getNewOrdersRequest();
	        $dateFrom = new DateTime();
	        $dateFrom->sub(new DateInterval('P30D'));
	        $request->setDateFrom($dateFrom);

	        $request->setCanceledState(Orders::STATE_ALL);


	        /** @var \SoluteSop\Api\Response\Orders $ordersResponse */
	        $ordersResponse = $this->getSopOrders(null, $request);

	        $countOrders = $ordersResponse->count();
	        if ($request->getDateFrom()) {
	            $this->log('Request by DateFrom ' . $request->getDateFrom()->format('Y-m-d H:i:s') . ' (' . $countOrders . ' Orders found)');
	        } else {
	            $this->log('Request for all Orders' . ' (' . $countOrders . ' Orders found)');
	        }

	        foreach ($ordersResponse->getAll() as $orderResponse) {
	            try {
	                $this->cancelSopOrder($orderResponse);
	            } catch (Exception $e) {
	                $this->log($e->getMessage());
	                continue;
	            }
	        }


	        return $this;

	    } catch (Exception $e) {
	        $this->log($e->getMessage());
	        throw new RuntimeException('Order::cancelOrders failed (' . $e->getMessage() . ')', null, $e);
	    }
	}
	/**
	 *
	 * @throws RuntimeException
	 * @return Solute_Sop_Model_Service_Order
	 */
	public function importNewSopOrders()
	{
		try {
            if (!$this->getConfig()->getApiEnabled()) {
                return $this;
            }

			$customerEmail = $this->getConfig()->getSyncCustomerEmail();

			$request = $this->getNewOrdersRequest();

			/** @var Mage_Core_Model_Date $cronLastRunStart */
			$cronLastRunStart = Mage::getModel('core/date');
			$cronLastRunStart->gmtDate();

			/**
			 * dateFrom by last call to collect just new orders
			 */
			$cronLastRun = $this->getCronLastRunVariable();
			/*
			if ($cronLastRun->getValue('plain')) {
				// start at last cron start
				$request->setDateFrom(new DateTime($cronLastRun->getValue('plain')));
			}
			*/

			#$request->setDateFrom(new DateTime('2017-03-10')); // older orders are not confirmed, but shipped
			$request->setConfirmationState(Orders::STATE_NONE); //just unconfirmed new orders
            $request->setCanceledState(Orders::STATE_NONE);

			#$request->setDateTo(new DateTime('2017-03-20'));

			/** @var \SoluteSop\Api\Response\Orders $ordersResponse */
			$ordersResponse = $this->getSopOrders(null, $request);

			$countOrders = $ordersResponse->count();
			if ($request->getDateFrom()) {
				$this->log('Request by DateFrom ' . $request->getDateFrom()->format('Y-m-d H:i:s') . ' (' . $countOrders . ' Orders found)');
			} else {
				$this->log('Request for all Orders' . ' (' . $countOrders . ' Orders found)');
			}

			foreach ($ordersResponse->getAll() as $orderResponse) {
				try {

					/** @var \SoluteSop\Api\Response\Order $orderResponse */
					$orderResponse->setEmail($customerEmail);

					// Confirm all in past START
					/*
					foreach ($orderResponse->getItems() as $item) {

						$updateRequest = $this->getNewOrderLineRequest();
						$updateRequest->setLineReference($item->getLineReference())->setQuantityConfirmed($item->getQuantityOrdered());
						Zend_Debug::dump($updateRequest);
						$this->getApiClient()->updateOrderLine($updateRequest);
					}
					*/

					// Confirm all in past ENDE

					$this->saveSopOrder($orderResponse);
					#Zend_Debug::dump($orderResponse);

				} catch (Exception $e) {
					$this->log($e->getMessage());
					continue;
				}
			}

			// set lastRun in core variable and save for next cron to start at this time
			$cronLastRun->setPlainValue($cronLastRunStart->gmtDate())->save();
			return $this;

		} catch (Exception $e) {
		    $this->log($e->getMessage());
			throw new RuntimeException('Order::importNewOrders failed (' . $e->getMessage() . ')', null, $e);
		}
	}

	/**
	 *
	 * @param Mage_Sales_Model_Order $order
	 * @return bool
	 */
	public function isSopOrder(Mage_Sales_Model_Order $order)
	{
	    /**
		 * second proof
		 * possible failure, by changing payment method
		 * old orders will not be identified
		 */
		$paymentMethod = $order->getPayment()->getMethod();
		if ($paymentMethod != $this->getConfig()->getSyncPaymentMethod()) {
			return false;
		}
		return true;
	}

	public function isSopQuote(Mage_Sales_Model_Quote $quote)
	{
		#$incrementId = $quote->getReservedOrderId();
        $paymentMethod = $quote->getPayment()->getMethod();
		if ($paymentMethod != $this->getConfig()->getSyncPaymentMethod()) {
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param \SoluteSop\Api\Response\Order $orderResonse
	 * @return Mage_Sales_Model_Order
	 */
	public function isSopOrderImported(\SoluteSop\Api\Response\Order $orderResponse)
	{
		$incrementId = self::getIncrementId($orderResponse);
		#$incrementId = 100050267;
		/** @var Mage_Sales_Model_Resource_Order_Collection $orderCollection */
		$orderCollection = Mage::getModel('sales/order')->getCollection();
		$orderCollection->addAttributeToFilter('increment_id', array('eq' => $incrementId));

		if (($order = $orderCollection->getFirstItem()) && $order->getId() > 0) {
			return $order;
		}
		return false;
	}

	public function cancelSopOrder(\SoluteSop\Api\Response\Order $orderResponse)
	{
	    try {

	        if (!($order = $this->isSopOrderImported($orderResponse))) {
	            $this->log('Order dosnt exists ' . $orderResponse->getOrderId() . ' and can no be cancelled');
	            return $this;
	        }

	        #$this->log('-------- cancelSopOrder Start ------------ ');

	        if ($order->canCancel()) {
	            $order->cancel();
	            $history = $order->addStatusHistoryComment('Sop Order Cancelled (id:' . $orderResponse->getOrderId() . ')', false);
	            $history->setIsCustomerNotified(false);
	            $order->save();

	            $this->log('Order ' . $order->getId() . ' successfully cancelled');
	        }

	        #$this->log('-------- cancelSopOrder End ------------ ');

	    } catch (Exception $e) {
	        $this->log($e->getMessage());
	        throw new RuntimeException('Sync::cancelSopOrder failed (' . $e->getMessage() . ')', null, $e);
	    }
	}

	public function saveSopOrder(\SoluteSop\Api\Response\Order $orderResponse)
	{
		try {

			$this->log('-------- saveSopOrder Start ------------ ');
			$this->log('ID: ' . $orderResponse->getOrderId() . ' CartReference: ' . $orderResponse->getCartReference() . ' Name: ' . $orderResponse->getShippingAddress1());

			if (($order = $this->isSopOrderImported($orderResponse))) {
				$this->log('Order exists ' . $order->getId() . ' ' . $order->getIncrementId());
				// TEST for CONFIRM muss wieder raus
				#$this->updateOrder($order, self::ORDER_STATE_CONFIRM);
				return $this;
			}

			$this->log('Order new');

			/** @var Solute_Sop_Model_Hydrator_SopOrderToSalesQuote $hydrator */
			$hydrator = Mage::getModel('solute_sop/hydrator_sopOrderToSalesQuote');
			$hydrator->setOrderService($this);

			#$currencyCode = Mage::getStoreConfig('solute_sop_setting/sync/currency_code');
			#$customerGroup = Mage::getStoreConfig('solute_sop_setting/sync/customer_group');

			$quote = $hydrator->hydrate($orderResponse);

			$additionalOrderData = array(
					'order_number_external' => $orderResponse->getOrderId(),
			);

			$shippingAmount = $quote->getShippingAddress()->getShippingAmount();
			$this->log('ShippingAmount ' . $shippingAmount);

			// Shipping Amount on this place
			$shippingAmount = 0;
			foreach ($orderResponse->getItems() as $item) {
				/** @var OrderItem $item */
				$shippingAmount += $item->getShippingAmount();
			}

			/**
			 * SHIPPING GEHT NICHT
			 */
			#$this->log('SHIPPINGPRICE GEHT NOCH NICHT DYNAMISCH');

			$quote->getShippingAddress()->setShippingAmount($shippingAmount);
            $quote->getShippingAddress()->setBaseShippingAmount($shippingAmount);

			// senseless hack for Magento
			#$quote->collectTotals();
			$quote->save();

			// needed to collect totals again with loaded items
			foreach ($quote->getAllAddresses() as $address) {
				$address->unsetData('cached_items_nonnominal');
				$address->unsetData('cached_items_nominal');
			}

			$this->log('Load Quote ' . $quote->getId() . ' again for collect totals');

			$quote->load($quote->getId());

			$quote
				->collectTotals()
				->save()
			;

            $this->log('Quote: ' . $quote->getId() . ' saved');

            $quote->getShippingAddress()->removeAllShippingRates();

			#$quote->getShippingAddress()->setShippingAmount($shippingAmount);
			#$quote->getShippingAddress()->setBaseShippingAmount($shippingAmount);
			#$quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
            #$quote->getShippingAddress()->setShippingAmount($shippingAmount);
            #$quote->getShippingAddress()->setBaseShippingAmount($shippingAmount);

			$this->log('ShippingAmount ' . $quote->getShippingAddress()->getShippingAmount());

            /** @var $quoteService Mage_Sales_Model_Service_Quote */
			$quoteService = Mage::getModel('sales/service_quote', $quote);
			$quoteService->setOrderData($additionalOrderData);
			$quoteService->submitAll();

            $history = $quoteService->getOrder()->addStatusHistoryComment('Sop Order Saved (id:' . $orderResponse->getOrderId() . ')', false);

			if ($history) {
                $history->save();
            }

            // set external Order Id
            $quoteService->getOrder()->setExtOrderId($orderResponse->getOrderId());
            $quoteService->getOrder()->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
            $quoteService->getOrder()
                ->setShippingAmount($shippingAmount)
                ->setBaseShippingAmount($shippingAmount)
                ->setShippingInclTax($shippingAmount)
                ->setBaseShippingInclTax($shippingAmount)
                ->setGrandTotal($quoteService->getOrder()->getGrandTotal() + $shippingAmount);
            ;
            $quoteService->getOrder()->save();


            $this->log('Order: ' . $quoteService->getOrder()->getId() . ' saved (' . $quoteService->getOrder()->getShippingAmount() . ' | ' . $shippingAmount. ')');

			#Zend_Debug::dump($quote->getPayment()->debug());
			// confirm order

			$this->log('Confirm OrderLines on import SOP API');

			// Direct handle confirmation status
			$this->updateOrder($quoteService->getOrder(), self::ORDER_STATE_CONFIRM);

			$this->log('-------- saveSopOrder End ------------ ');
			return $quoteService->getOrder();

		} catch (Exception $e) {
            $this->log($e->getMessage());
			throw new RuntimeException('Sync::saveSopOrder failed (' . $e->getMessage() . ')', null, $e);
		}
	}

	/**
	 *
	 * @return Mage_Core_Model_Variable
	 */
	public function getCronLastRunVariable()
	{
		if (!$this->_cronLastRunVariable instanceof Mage_Core_Model_Variable) {
			/** @var Mage_Core_Model_Variable $var */
			$var = Mage::getModel('core/variable');
			$var->loadByCode(self::VARIABLE_CODE_ORDER_CRON_LAST_RUN);
			if (!$var->getId() > 0) {
				$var->setCode(self::VARIABLE_CODE_ORDER_CRON_LAST_RUN)
					->setName(self::VARIABLE_CODE_ORDER_CRON_LAST_RUN)
				;
				$var->save();
			}
			$this->_cronLastRunVariable = $var;
		}
		return $this->_cronLastRunVariable;
	}

	/**
	 *
	 * @return Orders
	 */
	public function getNewOrdersRequest()
	{
		$value = new Orders();
		return $value;
	}
	/**
	 *
	 * @return Order
	 */
	public function getNewOrderRequest()
	{
		$value = new Order();
		return $value;
	}
	/**
	 *
	 * @return OrderLine
	 */
	public function getNewOrderLineRequest()
	{
		$value = new OrderLine();
		return $value;
	}

	/**
	 *
	 * @param Solute_Sop_Model_Api_Client $value
	 * @return Solute_Sop_Model_Service_Client
	 */
	public function setApiClient(Client $value)
	{
		$this->_apiClient = $value;
		return $this;
	}

	/**
	 *
	 * @return Client
	 */
	public function getApiClient()
	{
		if (!$this->_apiClient instanceof Client) {
			$this->_initApiClient();
		}

		return $this->_apiClient;
	}

	/**
	 * init client by magento system configuration
	 * @return Solute_Sop_Model_Service_Client
	 */
	protected function _initApiClient()
	{
		/** @var Client $client */
		$client = new Client();
		$client->setAccessToken(Mage::getStoreConfig('solute_sop_setting/api/access_token'));
		$client->setEndpoint(Mage::getStoreConfig('solute_sop_setting/api/endpoint'));
		$client->setVersion(Mage::getStoreConfig('solute_sop_setting/api/version'));
		$this->_apiClient = $client;
		return $this;
	}

}
