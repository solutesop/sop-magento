<?php
use SoluteSop\Api\Response\Orders;
use SoluteSop\Api\Request\Order;

/**
 *
 * @author askold
 *
 */
class Solute_Sop_Model_Service_Sync extends Solute_Sop_Model_Service_Abstract
{


	/**
	 * Sync Service ran by cron
	 *
	 * @throws RuntimeException
	 * @return Solute_Sop_Model_Service_Sync
	 */
	public function import()
	{
		try {
			$stores = Mage::app()->getStores();

			/** @var Mage_Core_Model_App_Emulation $appEmulation */
			$appEmulation = Mage::getSingleton('core/app_emulation');

			foreach ($stores as $store) {

			    $this->log('Store (' . $store->getName() . ') import start');

				/** @var Mage_Core_Model_Store $store */
				$currentAppEmulationInfo = $appEmulation->startEnvironmentEmulation($store->getId());

				/**
				 *
				 * @var Solute_Sop_Model_Service_Order $orderService
				 */
				$orderService = Mage::getModel('solute_sop/service_order');

				$orderService->importNewSopOrders();

				$appEmulation->stopEnvironmentEmulation($currentAppEmulationInfo);

				$this->log('Store (' . $store->getName() . ') import end');
			}

			return $this;

		} catch (Exception $e) {
			throw new RuntimeException('Sync::import failed (' . $e->getMessage() . ')', null, $e);
		}
	}


	public function cancel()
	{
	    try {
	        $stores = Mage::app()->getStores();

	        /** @var Mage_Core_Model_App_Emulation $appEmulation */
	        $appEmulation = Mage::getSingleton('core/app_emulation');

	        foreach ($stores as $store) {

	            $this->log('Store (' . $store->getName() . ') cancel start');

	            /** @var Mage_Core_Model_Store $store */
	            $currentAppEmulationInfo = $appEmulation->startEnvironmentEmulation($store->getId());

	            /**
	             *
	             * @var Solute_Sop_Model_Service_Order $orderService
	             */
	            $orderService = Mage::getModel('solute_sop/service_order');

	            $orderService->cancelSopOrders();

	            $appEmulation->stopEnvironmentEmulation($currentAppEmulationInfo);

	            $this->log('Store (' . $store->getName() . ') cancel end');
	        }

	        return $this;

	    } catch (Exception $e) {
	        throw new RuntimeException('Sync::cancel failed (' . $e->getMessage() . ')', null, $e);
	    }
	}



}