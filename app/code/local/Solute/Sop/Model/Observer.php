<?php
/**
 *
 * @desc
 *
 *
 */

class Solute_Sop_Model_Observer extends Varien_Object
{


	public function handleOrderAfterSave(Varien_Event_Observer $observer)
	{
		try {
			/** @var Mage_Sales_Model_Order $order */
			$order = $observer->getOrder();

			if (!$order instanceof Mage_Sales_Model_Order) {
				#Zend_Debug::dump('NO SOP ORDER');
				return $this;
			}

			/** @var Solute_Sop_Model_Service_Order $orderService */
			$orderService = Mage::getModel('solute_sop/service_order');
			$orderService->handleOrderUpdate($order);

			return $this;

		} catch (Exception $e) {
			Mage::log('handleOrderAfterSave failed (' . $e->getMessage() . ')', null, Solute_Sop_Model_Service_Abstract::LOG_FILE, true);
			#throw $e;
			return $this;

		}

	}



}

