<?php

use SoluteSop\Api\Response\Order;
use SoluteSop\Api\Response\OrderItem;

class Solute_Sop_Model_Hydrator_SopOrderToSalesQuote
{


	protected $_quote = null;

	protected $_store = null;

	protected $_orderService = null;

	/**
	 *
	 * @param Order $sopOrder
	 * @return Mage_Sales_Model_Quote
	 */
	public function hydrate(Order $orderResponse)
	{

		$store = $this->getStore();
		$quote = $this->getQuote($orderResponse);

		/*
		if ($quote->getId() > 0) {
			Zend_Debug::dump('Existing Quote ' . $quote->getId());
		} else {
			Zend_Debug::dump('New Quote');
		}
		*/

		// OrderId of SOP
		#$quote->setReservedOrderId($orderResponse->getOrderId());

		/**
		 * hydrate quote
		 */
		$quote->setCustomerIsGuest(1);
		$quote->setCustomerNoteNotify(0);
		$quote->setStoreId($store->getId());
		$quote->setCreatedAt($orderResponse->getCreatedAt()->format('Y-m-d H:i:s'));
		$quote->setQuoteCurrencyCode($this->getOrderService()->getConfig()->getSyncCurrencyCode());
        #Mage::log($this->getOrderService()->getConfig()->getSyncPaymentMethod(), null, 'solute_sop.log');
		$quote->getPayment()->importData(array('method' => $this->getOrderService()->getConfig()->getSyncPaymentMethod()));

		/**
		 * hydrate Customer
		 * @var Mage_Customer_Model_Customer $customer
		 */
		$customer = $this->_hydrateCustomer($orderResponse);
		#$quote->setCustomer($customer);
		$quote->assignCustomer($customer);

		/**
		 * hydrate Address
		 */
		$address = $this->_hydrateShippingAddress($orderResponse, $quote);

		#Zend_Debug::dump($address->debug());
		$quote->setShippingAddress($address);

		$this->_addQuoteItems($orderResponse, $quote);


		$shippingAmount = 0;
		foreach ($orderResponse->getItems() as $item) {
			/** @var OrderItem $item */
			$shippingAmount += $item->getShippingAmount();
		}

		$quote->getShippingAddress()->setShippingAmount($shippingAmount);
		#$quote->reserveOrderId();


		/**
		 * @todos
		 * (/) shipping method
		 * payment
		 * customer_group?
		 * currency
		 *
		 *
		 */




		return $quote;
	}

	protected function _addQuoteItems(Order $orderResponse, Mage_Sales_Model_Quote $quote)
	{
		/**
		 * real adding items
		 */


		foreach ($orderResponse->getItems() as $item) {
			/** @var OrderItem $item */
			/** @var Mage_Catalog_Model_Product $product */
			$product = Mage::getModel('catalog/product');
			$product->load($product->getIdBySku($item->getItemCode()));

			if (!$product->getId() > 0) {
				// placeholder produkt?
				throw new RuntimeException('Product with itemCode/sku ' . $item->getItemCode() . ' (' . $item->getItemTitle() . ') not found');
			}

			#Zend_Debug::dump($product->getFinalPrice());

			$request = new Varien_Object(array(
				'qty' => $item->getQuantityOrdered(),
			));

			// existing product
			$quoteItem = $quote->getItemByProduct($product);

			if ($quoteItem === false) {
				$quoteItem = $quote->addProduct($product, $request);
			}

			$quoteItem->setProductId($product->getId());

			$this->getOrderService()->setSopOrderLineReferenceToQuoteItem($quoteItem, $item);

			$quoteItem->setCustomPrice($item->getAmount())
				->setPrice($item->getAmount())
				->setOriginalPrice($item->getAmount())
				->setOriginalCustomPrice($item->getAmount()) // method nicht wirklich da, eventuell varien mist?
			;
		}


		return $this;
	}


	/**
	 *
	 * @param Order $orderResponse
	 * @param Mage_Sales_Model_Quote $quote
	 * @return Mage_Sales_Model_Quote_Address
	 */
	protected function _hydrateShippingAddress(Order $orderResponse, Mage_Sales_Model_Quote $quote)
	{
		// orderResponse has iso code 2
		#$country = Mage::getModel('directory/country')->loadByCode($orderResponse->getShippingCountry());
		#Zend_Debug::dump($country->getCountryId());

		/** @var Mage_Sales_Model_Quote_Address $address */
		$address = $quote->getShippingAddress();

		/**
		 * firstname, lastname split
		 */
		$firstname = $lastname = '';

		$addressLine1 = explode(' ', trim($orderResponse->getShippingAddress1()));
		$addressLine1 = array_reverse($addressLine1);
		$lastname = $addressLine1[0];
		unset($addressLine1[0]);
		foreach ($addressLine1 as $addressLine1Value) {
			$firstname .= $addressLine1Value;
		}

		$phoneNumber = $orderResponse->getShippingPhoneNumber() ? $orderResponse->getShippingPhoneNumber() : '0123456789';
		$state = $orderResponse->getShippingState() ? $orderResponse->getShippingState() : '';
		/**
		 * hydrate
		 */
		$address->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
			->setCustomerId($quote->getCustomer()->getId())
			->setCity($orderResponse->getShippingCity())
			->setFirstname($firstname)
			->setLastname($lastname)
			->setCompany($orderResponse->getShippingAddress2())
			->setStreet($orderResponse->getShippingAddress3())
			->setPostcode($orderResponse->getShippingPostalCode())
			->setTelephone($phoneNumber)
			#->setCountryId($country->getCountryId())
			->setCountryId($orderResponse->getShippingCountry())
			->setRegion($state)
		;
		$address->setSaveInAddressBook(0);

		#Zend_Debug::dump($this->getOrderService()->getConfig()->getSyncShippingMethod());
        $rate = $address->getShippingRateByCode($this->getOrderService()->getConfig()->getSyncShippingMethod());
        if (!$rate) {
            /** @var Mage_Sales_Model_Quote_Address_Rate $shippingRate */
            $shippingRate = Mage::getModel('sales/quote_address_rate');
            $shippingRate->setCode($this->getOrderService()->getConfig()->getSyncShippingMethod());
            $address->addShippingRate($shippingRate);
        }

		$address->setCollectShippingRates(true)
			->collectShippingRates()
			->setSameAsBilling(0)
			->setShippingMethod($this->getOrderService()->getConfig()->getSyncShippingMethod())
		;

        $this->getOrderService()->log($address->getShippingMethod());

		return $address;
	}

	/**
	 *
	 * @param Order $orderResponse
	 * @return Mage_Customer_Model_Customer
	 */
	protected function _hydrateCustomer(Order $orderResponse)
	{
		/** @var Mage_Customer_Model_Customer $customer */
		$customer = Mage::getModel('customer/customer');
		$customer->setStore($this->getStore());
		$customer->loadByEmail($orderResponse->getEmail());

		#Zend_Debug::dump($customer->debug());

		if (!$customer->getId() > 0) {
			$customer->setEmail($orderResponse->getEmail());
			$customer->save();
		}

		return $customer;
	}


	public function setQuote(Mage_Sales_Model_Quote $value)
	{
		$this->_quote = $value;
		return $this;
	}


	/**
	 *
	 * @return Mage_Sales_Model_Quote
	 */
	public function getQuote(Order $orderResponse)
	{
		if (!$this->_quote) {
			$quote = $this->_loadExistingQuote($orderResponse);
			if ($quote) {
				$this->_quote = $quote;
			} else {
				$incrementId = $this->getOrderService()->getIncrementId($orderResponse);
				$this->_quote = Mage::getModel('sales/quote');
				$this->_quote->setReservedOrderId($incrementId);
			}
		}
		return $this->_quote;
	}

	protected function _loadExistingQuote(Order $orderResponse)
	{
		// OrderId of SOP
		// SOP_101_123
		$incrementId = $this->getOrderService()->getIncrementId($orderResponse);

		$quote = Mage::getModel('sales/quote')->load($incrementId, 'reserved_order_id');

		return $quote->getId() > 0 ? $quote : false;
	}


	public function setStore(Mage_Core_Model_Store $value)
	{
		$this->_store = $value;
		return $this;
	}

	/**
	 *
	 * @return Mage_Core_Model_Store
	 */
	public function getStore()
	{
		if (!$this->_store) {
			$this->_store = Mage::app()->getStore();
		}
		return $this->_store;
	}


	public function setOrderService(Solute_Sop_Model_Service_Order $value)
	{
		$this->_orderService = $value;
		return $this;
	}
	public function getOrderService()
	{
		if (!$this->_orderService) {
			throw new UnexpectedValueException('OrderService not set. Use self::setOrderService(Solute_Sop_Model_Service_Order $value)');
		}
		return $this->_orderService;
	}

}
