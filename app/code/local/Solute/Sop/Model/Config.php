<?php

/**
 *
 * @author askold
 *
 * @method Solute_Sop_Model_Config setApiVersion(string $value)
 * @method string getApiVersion()
 *
 * @method Solute_Sop_Model_Config setApiAccessToken(string $value)
 * @method string getApiAccessToken()
 *
 * @method Solute_Sop_Model_Config setApiEnabled(int $value)
 * @method int getApiEnabled()
 *
 *
 *
 * @method Solute_Sop_Model_Config setSyncEnabled(int $value)
 * @method int getSyncEnabled()
 *
 * @method Solute_Sop_Model_Config setSyncPaymentMentod(string $value)
 * @method string getSyncPaymentMethod()
 *
 * @method Solute_Sop_Model_Config setSyncShippingMentod(string $value)
 * @method string getSyncShippingMethod()
 *
 * @method Solute_Sop_Model_Config setSyncCustomerGroup(string $value)
 * @method string getSyncCustomerGroup()
 *
 * @method Solute_Sop_Model_Config setSyncCurrencyCode(string $value)
 * @method string getSyncCurrencyCode()
 *
 * @method Solute_Sop_Model_Config setSyncCustomerEmail(string $value)
 * @method string getSyncCustomerEmail()
 *
 * @method Solute_Sop_Model_Config setSyncOrderIncrementIdPrefix(string $value)
 * @method string getSyncOrderIncrementIdPrefix()
 *
 * @method Solute_Sop_Model_Config setSyncOrderUseCartReferenceAsIncrementId(bool $value)
 * @method bool getSyncOrderUseCartReferenceAsIncrementId()
 */

class Solute_Sop_Model_Config extends Varien_Object
{
}
