# Solute SOP Api Client PHP

## Installation packagist.org

```
composer require solutesop/sop-api-client
```

## Usage of PHP Client Lib

instanciate the Client
```
$client = new \SoluteSop\Api\Client();
```
configuring the client (access token needed)
```
$client->setAccessToken('YOUT_ACCESS_TOKEN');
```
Instanciate a Request Object and configure it
```
$request = new \SoluteSop\Api\Request\Order();
$request->setOrderId(1);
```
Call API
```
$response = $client->getOrder($request);
```
Work with the response object


## Request Examples
Each Response Object has method getRawData()
this is the raw json response of SOP API

### basic usage
create a request Object for what you want
Order, Orders, OrderLine

Call Client method
getOrder($request), getOrders($request), updateOrderLine($request)
and get the Reponse

### Order Request example
single Order

```
$request = new \SoluteSop\Api\Request\Order();
$request->setOrderId(1);
/** @var \SoluteSop\Api\Response\Order $order */
$order = $client->getOrder($request);
```

Orders Request example
many orders filtered or not

```
$request = new \SoluteSop\Api\Request\Orders();
$request->setConfirmationState(\SoluteSop\Api\Request\Orders::STATE_NONE);
$request->setDateFrom(new DateTime('2017-01-01'));
/** @var \SoluteSop\Api\Response\Orders $orderCollection */
$orderCollection = $client->getOrders($request);
foreach ($orderCollection->getAll() as $order) {
    // do something
}
```

OrderLine
Update an orderLine example

```
$request = new \SoluteSop\Api\Request\OrderLine();
$request->setQuantityConfirmed(1)->setLineReference('001-1-1-1');
$response = $client->updateOrderLine($request);
if ($response->getError()) {
    echo $response->getError();
}
```
