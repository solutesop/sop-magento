<?php
namespace SoluteSop;

/**
 * 
 * @author:     Askold Beyer <askold.beyer@shopping.de>
 * @date:       20.03.17
 * @copyright   Copyright (c) 2017 Solute.de GmbH
 */

spl_autoload_register(array(new Autoloader(),'autoload'), true, true);

/**
 *
 * simulate PSR4 if not installed by composer
 *
 * Class Autoloader
 * @package SoluteSop
 */
class Autoloader
{
    public function autoload($class)
    {
        if (strpos($class, __NAMESPACE__) === false) {
            return false;
        }
        $class = str_replace(__NAMESPACE__, '', $class);
        $file = str_replace('\\', '/', $class) . '.php';
        $filePath = realpath(dirname(__FILE__)) . $file;

        if (file_exists($filePath)) {
            return include_once $filePath;
        }
        return false;
    }
}

