<?php
namespace SoluteSop\Api\Request;

use SoluteSop\Api\Response\ResponseInterface;
use Eloquent\Composer\Configuration\Element\RepositoryInterface;
use SoluteSop\Api\Hydrator\HydratorInterface;

abstract class RequestAbstract implements RequestInterface
{


	protected $_responseObject = null;


	protected $_responseClass = 'SoluteSop\Api\Response\Orders';


	/**
	 *
	 * @var string
	 */
	protected $_method = null;

	protected $_isPost = false;



	/**
	 * automatic call getter methods to build query
	 *
	 * key => getter var
	 *
	 * @var array
	 */
	protected $_buildGetParams = array();

	/**
	 *
	 * @var array
	 */
	protected $_buildPostParams = array();

	public function isPost($value = null)
	{
		if ($value !== null) {
			$this->_isPost = (bool)$value;
			return $this;
		}
		return (boolean) $this->_isPost;
	}

	public function modifyUri($uri)
	{
		return $uri;
	}

	public function buildGetParams()
	{
		$patternParams = $this->_buildGetParams;

		$params = $this->_buildParamsBefore(array(), $patternParams);
		$params = $this->_buildGetParamsBefore($params, $patternParams);
		$params = $this->_buildParams($params, $patternParams);
		$params = $this->_buildGetParamsAfter($params, $patternParams);
		$params = $this->_buildParamsAfter($params, $patternParams);
		return $params;
	}
	public function buildPostParams()
	{
		$patternParams = $this->_buildPostParams;

		$params = $this->_buildParamsBefore(array(), $patternParams);
		$params = $this->_buildPostParamsBefore($params, $patternParams);
		$params = $this->_buildParams($params, $patternParams);
		$params = $this->_buildPostParamsAfter($params, $patternParams);
		$params = $this->_buildParamsAfter($params, $patternParams);
		return $params;
	}

	protected function _buildParams(array $params = null, array $patternParams = null)
	{
		if (!$params) {
			$params = array();
		}

		foreach ($patternParams as $key => $getterKey) {
			$value = $this->_getParamValueByGetterKey($getterKey);
			if ($value !== null && $value !== "" && $value !== 0) {

				if ($value instanceof \DateTime) {
					$value = $value->format('Y-m-d H:i:s');
				}

				$params[$key] = $value;
			}
		}

		return $params;
	}

	protected function _getParamValueByGetterKey($getterKey)
	{
		$method = 'get' . ucfirst($getterKey);
		if (!method_exists($this, $method)) {
			throw new \RuntimeException('Method "' . $method . '" dosnt exists in ' . get_class($this));
		}

		return $this->$method();
	}

	/**
	 * override this
	 * @return array
	 */
	protected function _buildParamsBefore(array $params = null, array $patternParams = null)
	{
		return $params;
	}
	/**
	 * override this
	 * @return array
	 */
	protected function _buildParamsAfter(array $params = null, array $patternParams = null)
	{
		return $params;
	}

	/**
	 * override this
	 * @return array
	 */
	protected function _buildGetParamsBefore(array $params = null, array $patternParams = null)
	{
		return $params;
	}
	/**
	 * override this
	 * @return array
	 */
	protected function _buildGetParamsAfter(array $params = null, array $patternParams = null)
	{
		return $params;
	}

	/**
	 * override this
	 * @return array
	 */
	protected function _buildPostParamsBefore(array $params = null, array $patternParams = null)
	{
		return $params;
	}
	/**
	 * override this
	 * @return array
	 */
	protected function _buildPostParamsAfter(array $params = null, array $patternParams = null)
	{
		return $params;
	}


	/**
	 *
	 * @param string $value
	 * @return RequestAbstract
	 */
	public function setMethod($value)
	{
		$this->_method = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getMethod()
	{
		return $this->_method;
	}


	public function setReponseObject(ResponseInterface $value)
	{
		$this->_responseObject = $value;
		return $this;
	}
	public function getResponseObject()
	{
		if ($this->_responseObject === null) {
			$class = $this->getResponseClass();
			$response = new $class();
			$this->_responseObject = $response;
		}
		return $this->_responseObject;
	}


	public function setResponseClass($value)
	{
		$this->_responseClass = $value;
		return $this;
	}

	public function getResponseClass()
	{
		return $this->_responseClass;
	}

}