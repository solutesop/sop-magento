<?php
namespace SoluteSop\Api\Request;

use SoluteSop\Api\Response\ResponseInterface;

interface RequestInterface
{

	/**
	 *
	 */
	public function modifyUri($uri);

	public function buildGetParams();
	public function buildPostParams();

	public function isPost();

	public function getMethod();



	/**
	 * @return ResponseInterface
	 */
	public function getResponseObject();

}