<?php
namespace SoluteSop\Api\Response;

class Orders extends ResponseAbstract implements \Countable
{

	/**
	 *
	 * @var string
	 */
	protected $_hydratorClass = 'SoluteSop\Api\Response\Hydrator\Orders';

	/**
	 *
	 * @var array
	 */
	protected $_entities = array();


	public function getNewEmptyOrder()
	{
		return new Order();
	}


	public function add(Order $value)
	{
		$this->_entities[$value->getOrderId()] = $value;
	}
	public function getAll()
	{
		return $this->_entities;
	}

	public function count()
	{
		return count($this->_entities);
	}
}