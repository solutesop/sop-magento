<?php
namespace SoluteSop\Api\Response;

use SoluteSop\Api\Response\Hydrator\HydratorInterface;

interface ResponseInterface
{

	public function setRawData($value);
	public function getRawData();

	/**
	 * @return HydratorInterface
	 */
	public function getHydratorObject();

}