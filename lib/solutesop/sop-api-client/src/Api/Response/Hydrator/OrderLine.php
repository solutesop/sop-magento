<?php
namespace SoluteSop\Api\Response\Hydrator;


use SoluteSop\Api\Response\OrderLine as OrderLineResponse;

class OrderLine extends HydratorAbstract
{

	protected function _hydrate(OrderLineResponse $response)
	{
        if ($response->getRawData() == 'true') {
            $response->setError(false);
        }

        $rawData = json_decode($response->getRawData());

        if (isset($rawData->error)) {
            $response->setError($rawData->error);
        }
		#\Zend_Debug::dump($response->getRawData());
		return $this;
	}

}
