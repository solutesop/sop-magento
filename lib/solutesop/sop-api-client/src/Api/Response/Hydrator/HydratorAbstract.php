<?php
namespace SoluteSop\Api\Response\Hydrator;

use SoluteSop\Api\Response\ResponseInterface;

abstract class HydratorAbstract implements HydratorInterface
{


	public function hydrate(ResponseInterface $response)
	{
		return $this->_hydrate($response);
	}


}