<?php
namespace SoluteSop\Api\Response\Hydrator;

use SoluteSop\Api\Request\RequestInterface;
use SoluteSop\Api\Response\ResponseInterface;

interface HydratorInterface
{

	/**
	 *
	 * @param RequestInterface $request
	 * @return ResponseInterface
	 */
	public function hydrate(ResponseInterface $response);

}