# Magento Extension Solute SOP
solute.de GmbH Zeppelinstraße 15 D-76185 Karlsruhe

# Installation

## Composer

{
    "name":"sop-magento-test",
    "description": "Solute SOP Magento Extension",
    "type":"magento-module",
    "authors":[
        {
            "name":"Askold Beyer",
            "email":"askold.beyer@shopping.de"
        }
    ],
    "require":{
        "solutesop/sop-magento":"dev-master",
        "solutesop/sop-api-client":"dev-master"
    },
    "extra":{
        "magento-root-dir": "./",
        "auto-append-gitignore": true,
        "magento-deploystrategy": "symlink"
    }
}

## Download

https://bitbucket.org/solutesop/sop-magento/downloads/
Zip Datei en Magento Root Directory entpacken

## Konfiguration

System > Konfiguration > Verkäufe > Solute SOP

### Solute SOP API

1. Store auswählen
2. Solute SOP API aktivieren
3. API Access Token (erhältlich über Vertriebsteam) eintragen

### Solute SOP Synchronisation

1. aktivieren
2. email-Adresse des Kunden angeben, in dem die Bestellungen einlaufen sollen
    dieser Kunde sollte solute.de GmbH als Rechnungs Adresse hinterlegt haben
    hat den Hintergrund, dass Sie die Bestellungen an diese Rechnungsadresse weiterleiten, damit sie Ihr Geld bekommen
3. Zahlungsart (Solute SOP Payment - nicht ändern)
4. Versandart (Tablerate)
5. CurrencyCode EUR
6. CustomerGroup (keine Relevanz derzeitig)
7. Order Increment ID prefix. Bestellungen werden damit identifiziert, increment_id wird damit beschrieben (bestenfalls so lassen)

Jeder Store kann einen eigenen Login haben. Sollten mehere Stores verwaltet werden, dann sollte der Store explizit gewählt werden. Die Einstellungen in der Standardkonfiguration würde bedeuten, dass das alle Stores durchlaufen werden und dann für jeden die selben Einstellungen gelten. Heißt, die Bestellungen könnten mehrfach importiert werden.
Synchronisation läuft mit StoreEmulation.

# Debugging

- log Datei unter var/log/solute_sop.log

# Funktionsweise

- Import über Magento Cron (5 minütlich wird geschaut, ob es neue Bestellungen gibt)
- Bestellungen die abgeholt wurden, werden Confirmed

# TroubleShooting

## Bestellung wird nicht angelegt
- Extension ist konfiguriert?
- Referenzkunde nicht vorhanden (Solute SOP Synchronisation - Punkt 2.)
- Referenzkunde Rechnungsadresse nicht vorhanden/valide
- Bestellte Artikel lagernd (saleable) ? Problem könnte umgangen werden, in dem global einstellt wird, dass Produkte ohne Bestand verkaufbar sind
